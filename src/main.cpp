#include <stdio.h>

#include "SDL2/SDL.h"

enum class ConnectionPoint {
  Left = 0,
  Right,
  Bottom,
  Top,

  Left1 = 0,
  Right1,
  Left2,
  Right2,
  Left3,
  Right3,
  Left4,
  Right4,

  COUNT,
  None
};

enum class ConnectionType {
  None,
  Door,
  Lock,
};

struct Room;

struct Connection {
  Room* room;
  ConnectionType c_type;
  bool is_available;
};

int const CellSize = 16;

#define ROOM_TYPES(ENTRY) \
  ENTRY(Airlock) \
  ENTRY(Barracks) \
  ENTRY(Bay) \
  ENTRY(Command) \
  ENTRY(Dock) \
  ENTRY(Elevator) \
  ENTRY(Gym) \
  ENTRY(Hangar) \
  ENTRY(Lounge) \
  ENTRY(MechConstruction) \
  ENTRY(Mess) \
  ENTRY(Mining) \
  ENTRY(Processing) \
  ENTRY(ResearchHz) \
  ENTRY(ResearchVt) \
  ENTRY(ShipConstruction) \
  ENTRY(Stairway) \

#define TO_ENUM(name) name,
#define TO_STRING(name) #name,

enum class RoomType {
  ROOM_TYPES(TO_ENUM)

  COUNT
};

char const* getRoomTypeName(RoomType rt) {
  char const* names [] = {
    ROOM_TYPES(TO_STRING)
  };
  return names[(int)rt];
}

SDL_Color getRoomTypeColor(RoomType rt) {
  switch (rt) {
    case RoomType::Airlock:          return { 0x00, 0x00, 0x88 };
    case RoomType::Barracks:         return { 0x00, 0x00, 0xFF };
    case RoomType::Bay:              return { 0x00, 0x88, 0x00 };
    case RoomType::Command:          return { 0x00, 0x88, 0x88 };
    case RoomType::Dock:             return { 0x00, 0x88, 0xFF };
    case RoomType::Elevator:         return { 0x00, 0xFF, 0x00 };
    case RoomType::Gym:              return { 0x00, 0xFF, 0x88 };
    case RoomType::Hangar:           return { 0x00, 0xFF, 0xFF };
    case RoomType::Lounge:           return { 0x88, 0x00, 0x00 };
    case RoomType::MechConstruction: return { 0x88, 0x00, 0x88 };
    case RoomType::Mess:             return { 0x88, 0x00, 0xFF };
    case RoomType::Mining:           return { 0x88, 0x88, 0x00 };
    case RoomType::Processing:       return { 0x88, 0x88, 0x88 };
    case RoomType::ResearchHz:       return { 0x88, 0x88, 0xFF };
    case RoomType::ResearchVt:       return { 0x88, 0xFF, 0x00 };
    case RoomType::ShipConstruction: return { 0x88, 0xFF, 0x88 };
    case RoomType::Stairway:         return { 0x88, 0xFF, 0xFF };
    default: return { 0xFF, 0xFF, 0xFF };
  }
}

struct Room {
  RoomType r_type;
  bool is_initialized;
  int x, y, w, h;
  int num_connections;
  int side;
  union {
    struct {
      Connection left, right, bottom, top;
    };
    struct {
      Connection left1, right1, left2, right2, left3, right3, left4, right4;
    };
    Connection connections [(int)ConnectionPoint::COUNT];
  };
};

void initRoomConnections(Room* room, int num_connections) {
  room->num_connections = num_connections;
  for (int i = 0; i < room->num_connections; ++i) {
    room->connections[i].is_available = true;
  }
}

void printRoom(Room* room) {
  printf("Room(%s, (%d,%d,%d,%d), num_connections:%d, side:%d)\n", getRoomTypeName(room->r_type), room->x, room->y, room->w, room->h, room->num_connections, room->side);
  printf("  Connections[%d", room->connections[0].is_available);
  for (int i = 1; i < room->num_connections; ++i) {
    printf(",%d", room->connections[i].is_available);
  }
  printf("]\n");
}

void initRoom(Room* room, RoomType rt) {
  *room = { rt };
  switch (room->r_type) {
    case RoomType::Command:    room->w = 2; room->h = 2; initRoomConnections(room,2); break;
    case RoomType::Barracks:   room->w = 3; room->h = 1; initRoomConnections(room,2); break;
    case RoomType::ResearchHz: room->w = 2; room->h = 1; initRoomConnections(room,2); break;
    case RoomType::Processing: room->w = 3; room->h = 2; initRoomConnections(room,2); break;
    case RoomType::Airlock:    room->w = 1; room->h = 1; initRoomConnections(room,2); break;
    case RoomType::Dock:       room->w = 1; room->h = 1; initRoomConnections(room,2); break;
    case RoomType::Gym:        room->w = 4; room->h = 1; initRoomConnections(room,2); break;
    case RoomType::Lounge:     room->w = 3; room->h = 1; initRoomConnections(room,2); break;
    case RoomType::Mess:       room->w = 4; room->h = 1; initRoomConnections(room,2); break;

    case RoomType::ResearchVt: room->w = 1; room->h = 2; initRoomConnections(room,4); break;
    case RoomType::Stairway:   room->w = 1; room->h = 2; initRoomConnections(room,4); break;
    case RoomType::Elevator:   room->w = 1; room->h = 1; initRoomConnections(room,4); break;

    case RoomType::Bay:              room->w = 2; room->h = 4; initRoomConnections(room,6); break;
    case RoomType::Hangar:           room->w = 8; room->h = 6; initRoomConnections(room,4); break;
    case RoomType::MechConstruction: room->w = 2; room->h = 5; initRoomConnections(room,4); break;
    case RoomType::ShipConstruction: room->w = 8; room->h = 7; initRoomConnections(room,4); break;

    case RoomType::Mining: room->w = 4; room->h = 4; initRoomConnections(room,8); break;

    default: break;
  }
  printRoom(room);
}

bool setRoomConnection(Room* room, ConnectionPoint cp, Connection cnx) {
  switch (room->r_type) {
    case RoomType::Command:
    case RoomType::Barracks:
    case RoomType::ResearchHz:
    case RoomType::Processing:
    case RoomType::Gym:
    case RoomType::Lounge:
    case RoomType::Mess: {
      switch (cp) {
        case ConnectionPoint::Left:  room->left  = cnx; break;
        case ConnectionPoint::Right: room->right = cnx; break;
        default: return false;
      }
    } break;

    case RoomType::Airlock:
    case RoomType::Dock: {
      if (room->side != 0) {
        switch (cp) {
          case ConnectionPoint::Left:  room->left  = cnx; room->side = -1; break;
          case ConnectionPoint::Right: room->right = cnx; room->side =  1; break;
          default: return false;
        }
      } else {
        return false;
      }
    } break;

    case RoomType::ResearchVt:
    case RoomType::Stairway: {
      switch (cp) {
        case ConnectionPoint::Left2:  room->left2  = cnx; break;
        case ConnectionPoint::Right2: room->right2 = cnx; break;
        case ConnectionPoint::Left1:  room->left1  = cnx; break;
        case ConnectionPoint::Right1: room->right1 = cnx; break;
        default: return false;
      }
    } break;

    case RoomType::Bay:
    case RoomType::Hangar:
    case RoomType::MechConstruction:
    case RoomType::ShipConstruction: {
      switch (cp) {
        case ConnectionPoint::Left3:  room->left3  = cnx; break;
        case ConnectionPoint::Left2:  room->left2  = cnx; break;
        case ConnectionPoint::Left1:  room->left1  = cnx; break;
        case ConnectionPoint::Right3: room->right3 = cnx; break;
        case ConnectionPoint::Right2: room->right2 = cnx; break;
        case ConnectionPoint::Right1: room->right1 = cnx; break;
        default: return false;
      }
    } break;

    case RoomType::Mining: {
      if (room->side > 0) {
        switch (cp) {
          case ConnectionPoint::Right4: room->right4 = cnx; break;
          case ConnectionPoint::Right3: room->right3 = cnx; break;
          case ConnectionPoint::Right2: room->right2 = cnx; break;
          case ConnectionPoint::Right1: room->right1 = cnx; break;
          default: return false;
        }
      } else if (room->side < 0) {
        switch (cp) {
          case ConnectionPoint::Left4: room->left4 = cnx; break;
          case ConnectionPoint::Left3: room->left3 = cnx; break;
          case ConnectionPoint::Left2: room->left2 = cnx; break;
          case ConnectionPoint::Left1: room->left1 = cnx; break;
          default: return false;
        }
      } else {
        switch (cp) {
          case ConnectionPoint::Left4:  room->left4  = cnx; room->side = -1; break;
          case ConnectionPoint::Left3:  room->left3  = cnx; room->side = -1; break;
          case ConnectionPoint::Left2:  room->left2  = cnx; room->side = -1; break;
          case ConnectionPoint::Left1:  room->left1  = cnx; room->side = -1; break;
          case ConnectionPoint::Right4: room->right4 = cnx; room->side =  1; break;
          case ConnectionPoint::Right3: room->right3 = cnx; room->side =  1; break;
          case ConnectionPoint::Right2: room->right2 = cnx; room->side =  1; break;
          case ConnectionPoint::Right1: room->right1 = cnx; room->side =  1; break;
          default: return false;
        }
      }
    } break;

    case RoomType::Elevator: {
      switch (cp) {
        case ConnectionPoint::Left:   room->left   = cnx; break;
        case ConnectionPoint::Right:  room->right  = cnx; break;
        case ConnectionPoint::Top:    room->top    = cnx; break;
        case ConnectionPoint::Bottom: room->bottom = cnx; break;
        default: return false;
      }
    } break;

    default: return false;
  }

  return true;
}

bool connectRooms(Room* room_from, ConnectionPoint point_from,
                  Room* room_to,   ConnectionPoint point_to) {
  // Set an initial position for the room being connected
  room_to->x = room_from->x;
  room_to->y = room_from->y;

  // Offset the position based on where the starting rooms connection point is
  switch (point_from) {
    case ConnectionPoint::Left1:  // or Left
      room_to->x -= room_to->w;
      break;
    case ConnectionPoint::Right1: // or Right
      room_to->x += room_from->w;
      break;
    case ConnectionPoint::Left2:  // or Bottom
      if (room_from->r_type == RoomType::Elevator) {
        room_to->y += room_to->h;
      } else {
        room_to->x -= room_to->w;
        room_to->y -= 1;
      }
      break;
    case ConnectionPoint::Right2: // or Top
      if (room_from->r_type == RoomType::Elevator) {
        room_to->y -= room_from->h;
      } else {
        room_to->x += room_from->w;
        room_to->y -= 1;
      }
      break;
    case ConnectionPoint::Left3:
      room_to->x -= room_to->w;
        room_to->y -= 2;
      break;
    case ConnectionPoint::Right3:
      room_to->x += room_from->w;
        room_to->y -= 2;
      break;
    case ConnectionPoint::Left4:
      room_to->x -= room_to->w;
        room_to->y -= 3;
      break;
    case ConnectionPoint::Right4:
      room_to->x += room_from->w;
        room_to->y -= 3;
      break;
    default: break;
  }

  // Offset the position based on where its connection point is
  switch (point_to) {
    case ConnectionPoint::Left2:
    case ConnectionPoint::Right2: room_to->y += 1; break;
    case ConnectionPoint::Left3:
    case ConnectionPoint::Right3: room_to->y += 2; break;
    case ConnectionPoint::Left4:
    case ConnectionPoint::Right4: room_to->y += 3; break;
    default: break;
  }
  Connection cnx_from = { room_to,   ConnectionType::None, false };
  Connection cnx_to   = { room_from, ConnectionType::None, false };
  return setRoomConnection(room_from, point_from, cnx_from)
      && setRoomConnection(room_to,   point_to,   cnx_to);
}

Room* generateMechFactory(int const room_limit) {
  int num_room = 1;
  // 1 - create starting room, set it as the current room
  Room* root = (Room*)malloc(room_limit * sizeof(Room));
  initRoom(root, RoomType::MechConstruction);
  root->x = 0;
  root->y = 0;

  while (num_room < room_limit) {
    // 2 - pick a connection to build from
    int num_connections = 0;
    int count = 0;
    for (int i = 0; i < num_room; ++i) {
      Room& room = root[i];
      for (int j = 0; j < room.num_connections; ++j) {
        num_connections += room.connections[j].is_available;
      }
    }
    ConnectionPoint cp_list [num_connections];
    Room* r_list [num_connections];
    for (int i = 0; i < num_room; ++i) {
      Room* room = &root[i];

      switch (room->r_type) {
        case RoomType::Airlock:
        case RoomType::Dock:
          if (room->side != 0) {
            continue;
          }
          //NOTE(Seth): this is meant to fall through,
          // so the left|right decision isn't duplicated.
        case RoomType::Command:
        case RoomType::Barracks:
        case RoomType::ResearchHz:
        case RoomType::Processing:
        case RoomType::Gym:
        case RoomType::Lounge:
        case RoomType::Mess: {
          if (room->left.is_available)  { cp_list[count] = ConnectionPoint::Left;  r_list[count++] = room; }
          if (room->right.is_available) { cp_list[count] = ConnectionPoint::Right; r_list[count++] = room; }
        } break;

        case RoomType::ResearchVt:
        case RoomType::Stairway: {
          if (room->left1.is_available)  { cp_list[count] = ConnectionPoint::Left1;  r_list[count++] = room; }
          if (room->right1.is_available) { cp_list[count] = ConnectionPoint::Right1; r_list[count++] = room; }
          if (room->left2.is_available)  { cp_list[count] = ConnectionPoint::Left2;  r_list[count++] = room; }
          if (room->right2.is_available) { cp_list[count] = ConnectionPoint::Right2; r_list[count++] = room; }
        } break;

        case RoomType::Bay:
        case RoomType::Hangar:
        case RoomType::MechConstruction:
        case RoomType::ShipConstruction: {
          if (room->left1.is_available)  { cp_list[count] = ConnectionPoint::Left1;  r_list[count++] = room; }
          if (room->right1.is_available) { cp_list[count] = ConnectionPoint::Right1; r_list[count++] = room; }
          if (room->left2.is_available)  { cp_list[count] = ConnectionPoint::Left2;  r_list[count++] = room; }
          if (room->right2.is_available) { cp_list[count] = ConnectionPoint::Right2; r_list[count++] = room; }
          if (room->left3.is_available)  { cp_list[count] = ConnectionPoint::Left3;  r_list[count++] = room; }
          if (room->right3.is_available) { cp_list[count] = ConnectionPoint::Right3; r_list[count++] = room; }
        } break;

        case RoomType::Mining: {
          switch (room->side) {
            case -1: {
              if (room->left1.is_available)  { cp_list[count] = ConnectionPoint::Left1; r_list[count++] = room; }
              if (room->left2.is_available)  { cp_list[count] = ConnectionPoint::Left2; r_list[count++] = room; }
              if (room->left3.is_available)  { cp_list[count] = ConnectionPoint::Left3; r_list[count++] = room; }
              if (room->left4.is_available)  { cp_list[count] = ConnectionPoint::Left4; r_list[count++] = room; }
            } break;
            case  0: {
              if (room->left1.is_available)  { cp_list[count] = ConnectionPoint::Left1;  r_list[count++] = room; }
              if (room->right1.is_available) { cp_list[count] = ConnectionPoint::Right1; r_list[count++] = room; }
              if (room->left2.is_available)  { cp_list[count] = ConnectionPoint::Left2;  r_list[count++] = room; }
              if (room->right2.is_available) { cp_list[count] = ConnectionPoint::Right2; r_list[count++] = room; }
              if (room->left3.is_available)  { cp_list[count] = ConnectionPoint::Left3;  r_list[count++] = room; }
              if (room->right3.is_available) { cp_list[count] = ConnectionPoint::Right3; r_list[count++] = room; }
              if (room->left4.is_available)  { cp_list[count] = ConnectionPoint::Left4;  r_list[count++] = room; }
              if (room->right4.is_available) { cp_list[count] = ConnectionPoint::Right4; r_list[count++] = room; }
            } break;
            case  1: {
              if (room->right1.is_available) { cp_list[count] = ConnectionPoint::Right1; r_list[count++] = room; }
              if (room->right2.is_available) { cp_list[count] = ConnectionPoint::Right2; r_list[count++] = room; }
              if (room->right3.is_available) { cp_list[count] = ConnectionPoint::Right3; r_list[count++] = room; }
              if (room->right4.is_available) { cp_list[count] = ConnectionPoint::Right4; r_list[count++] = room; }
            } break;
          }
        } break;

        case RoomType::Elevator: {
          if (room->left.is_available)   { cp_list[count] = ConnectionPoint::Left;   r_list[count++] = room; }
          if (room->right.is_available)  { cp_list[count] = ConnectionPoint::Right;  r_list[count++] = room; }
          if (room->bottom.is_available) { cp_list[count] = ConnectionPoint::Bottom; r_list[count++] = room; }
          if (room->top.is_available)    { cp_list[count] = ConnectionPoint::Top;    r_list[count++] = room; }
        } break;

        default: break;
      }
    }

    // 3 - pick a room that can be built from that point
    int cnx_num = rand() % num_connections;

    // 4 - create the new room and connect it to the current room
    Room* new_room = &root[num_room++];
    initRoom(new_room, (RoomType)(rand() % (int)RoomType::COUNT));
    connectRooms(r_list[cnx_num], cp_list[cnx_num], new_room, (ConnectionPoint)(rand() % new_room->num_connections));

    // 5 - if the room limit is reached, end loop; otherwise, return to step 2
  }

  // 6 - exit the function
  return root;
}

void renderRooms(SDL_Renderer* renderer, Room rooms [], int num_rooms, int x = 0, int y = 0) {
  SDL_assert(rooms);
  for (int i = 0; i < num_rooms; ++i) {
    SDL_Color c = getRoomTypeColor(rooms[i].r_type);
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, 1);
    SDL_Rect rect = {
      CellSize * rooms[i].x + x,
      CellSize * rooms[i].y + y,
      CellSize * rooms[i].w,
      CellSize * rooms[i].h
    };
    SDL_RenderDrawRect(renderer, &rect);
  }
}

struct Grid {
  int w, h;
  bool* array;
};

void initGrid(Grid* grid, int w, int h) {
  grid->w = w;
  grid->h = h;
  grid->array = (bool*)malloc(grid->w * grid->h * sizeof(bool));
  for (int i = 0; i < w * h; ++i) {
    grid->array[i] = false;
  }
}

void destroyGrid(Grid* grid) {
  free(grid->array);
}

inline
bool getGridCell(Grid* grid, int x, int y) {
  return grid->array[x + y * grid->w];
}

inline
void setGridCell(Grid* grid, int x, int y, bool value) {
  grid->array[x + y * grid->w] = value;
}

Grid* createGridFromRooms(Room rooms [], int num_rooms) {
  int min_x = 0, max_x = 0;
  int min_y = 0, max_y = 0;
  for (int i = 0; i < num_rooms; ++i) {
    if (rooms[i].x < min_x) { min_x = rooms[i].x; }
    if (rooms[i].x + rooms[i].w > max_x) { max_x = rooms[i].x + rooms[i].w; }
    if (rooms[i].y < min_y) { min_y = rooms[i].y; }
    if (rooms[i].y + rooms[i].h > max_y) { max_y = rooms[i].y + rooms[i].h; }
  }
  Grid* grid = (Grid*)malloc(sizeof(Grid));
  initGrid(grid, max_x - min_x, max_y - min_y);
  for (int i = 0; i < num_rooms; ++i) {
    for (int y = 0; y < rooms[i].h; ++y) {
      for (int x = 0; x < rooms[i].w; ++x) {
        setGridCell(grid, rooms[i].x + x - min_x, rooms[i].y + y - min_y, true);
      }
    }
  }
  return grid;
}

void shutdown(SDL_Window* window, SDL_Renderer* renderer) {
  if (renderer) {
    SDL_DestroyRenderer(renderer);
  }
  if (window) {
    SDL_DestroyWindow(window);
  }
  SDL_Quit();
}

int const ScreenWidth  = 1280;
int const ScreenHeight = 960;

int main() {
  SDL_Init(SDL_INIT_VIDEO);

  SDL_Window* window;
  SDL_Renderer* renderer;

  if (SDL_CreateWindowAndRenderer(ScreenWidth, ScreenHeight, 0, &window, &renderer) < 0) {
    printf("ERROR: %s\n", SDL_GetError());
    shutdown(window, renderer);
    return -1;
  }

  int num_rooms = 10;
  Room* root = generateMechFactory(num_rooms);
  Grid* grid = createGridFromRooms(root, num_rooms);

  bool running = true;

  while (running) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_WINDOWEVENT: {
          switch (event.window.event) {
            case SDL_WINDOWEVENT_CLOSE: {
              running = false;
            } break;
          }
        } break;
        case SDL_KEYDOWN: {
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE: {
              running = false;
            } break;
          }
        } break;
        case SDL_MOUSEMOTION: {
          //TODO(Seth): keep track of the mouse position
        } break;
        default: {
          SDL_Log("Window %d got unknown event %d", event.window.windowID, event.window.event);
        } break;
      }
    }

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);

    for (int i = 0; i < (int)RoomType::COUNT; ++i) {
      SDL_Color c = getRoomTypeColor((RoomType)i);
      SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, 1);
      SDL_Rect rect = { 8 + (i/3) * 16, 8 + (i%3) * 16, 8, 8 };
      SDL_RenderFillRect(renderer, &rect);
    }

    for (int y = 0; y < grid->h; ++y) {
      for (int x = 0; x < grid->w; ++x) {
        SDL_SetRenderDrawColor(renderer, 1, 1, 1, 1);
        SDL_Rect rect = { 8 + x * 8, (ScreenHeight >> 1) + (8 + y * 8), 8, 8 };
        if (getGridCell(grid, x, y)) {
          SDL_RenderFillRect(renderer, &rect);
        } else {
          SDL_RenderDrawRect(renderer, &rect);
        }
      }
    }

    renderRooms(renderer, root, num_rooms, ScreenWidth / 2, ScreenHeight / 2);

    SDL_RenderPresent(renderer);
  }

  destroyGrid(grid);
  free(grid);
  free(root);

  shutdown(window, renderer);
	return 0;
}